# Instructions to build Session for AARCH64:
## Warning: These instructions are a work in progress and currently I have been unable to successfully cross compile Session for the PinePhone.
### Background:
Session is my favorite messenger and it's what I use to communicate with friends and family. It's a fork of Signal, an amazingly private messenger, which when stopped "couldn’t provide any useful information ("Session is my favorite messenger and it's what I use to communicate with friends and family. It's a fork of Signal, an amazingly private messenger, which when issued a warrant, Signal "couldn’t provide any" useful information ("Grand jury..."). The only information Signal collects is "Unix timestamps for when each account was created and the date that each account last connected to the Signal service" ("Grand jury...").

Session takes Signal's industry leading privacy to a level that meets any thread model. "Session is a decentralized messenger that supports completely private, secure, and anonymous communications" ("Lightpaper"). Everything is onion routed, end-to-end encrypted and your Session ID is a randomly generated string which cannot be traced back to you. It hides your IP not only from governments, bad actors, and Session itself, but also from the other users on Session. Your message takes 10-12 hops around the world, each with its own encryption layer, before reaching the recipient. This is not to mention the fact that Session is astonishingly low latency for taking so many hops, and it has a very nice UI that anyone could use intuitively. This is why I have been transitioning from SMS to Session for all my chats, but, there is one issue: there is no Linux Arm release available, and I am using a PinePhone, so I need to recompile Session for my phone.
I decided to use Distrobox, a tool I have worked on and is very good for situations like this. It allows you to "use any linux distribution inside your terminal" and is an amazing way to compile an application on Ubuntu without using a Ubuntu VM (89luca89).
### Attempt 1:
``` bash
distrobox create --name ub2204 --image docker.io/library/ubuntu:22.04
rm -rf ~/.nvm
distrobox enter ub2204

sudo apt update # ignore errors
sudo apt upgrade # ignore errors
sudo apt install curl python2 git-lfs cmake g++ # ignore errors

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
```
Then I entered the Session directory and checked out V1.7.7, and made the following changes: change build release to: "build-release": "cross-env SIGNAL_ENV=production electron-builder --config.extraMetadata.environment=production --publish=never --config.directories.output=release --arm64", Change the URL for better-sqlite3 to https://github.com/JoshuaWise/better-sqlite3 Add the following to files:
``` json
"ts/**/*.ts",
"ts/util/*.ts",
"./ts/**/*.ts",
"./ts/*.ts",
"./ts/util/*.ts",
"./ts/**/*",
```
Change Linux target to "appimage"
Then from within the Distrobox shell, I ran the following commands:
``` bash
alias python='/usr/bin/python2'
nvm install
nvm use
npm install -g yarn    # make sure yarn is up to date
rm yarn.lock
yarn install --frozen-lockfile # got error of frozen file not updated, fixed by deleting yarn.lock then rerun
yarn generate
yarn build-release --deb-no-default-config-files
exit
# Coppy appimage to PP
# Clean up
distrobox rm ub2204
rm -rf ~/.nvm
```
---
From here I have a releases/session-desktop-linux-arm64-1.7.7.AppImage which I scp over to my phone and can now run Session on my phone! However, I am not sure why, but the binary is not compiling correctly. I tried this in a VM as well, and while in fact it gave a different error, it was still not compiling correctly, so I believe my next step is to try and run Session from Waydroid. This is an ongoing experiment and please check back for more updates!

### Sources:
89luca89. “89LUCA89/Distrobox: Use Any Linux Distribution inside Your Terminal. Enable Both Backward and Forward Compatibility with Software and Freedom to Use Whatever Distribution You're More Comfortable with.” GitHub, https://github.com/89luca89/distrobox. “Grand Jury Subpoena for Signal User Data, Central District of California (Again!).” Signal Messenger, https://signal.org/bigbrother/cd-california-grand-jury/. Lightpaper — Session Private Messenger - Getsession.org. https://getsession.org/lightpaper/pdf.



