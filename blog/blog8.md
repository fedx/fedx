# I Use(d) Arch (on my phone) BTW

So, I have recently started daily driving a PinePhone as my only phone, and it is an interesting experience to say the least. So, here are my thoughts, and feelings on the whole subject.

## The Good:

* It's Linux on a phone!
* It's hackable and extendible with replaceable parts and an I2C accessory port for things like keyboards.
* It's a $200 phone!

## The Bad:

* It's a $200 phone, and it's super slow.
* Texting and calls are still a bit buggy (but that is partly my own fault).

## The Ugly:

* It's a niche platform and software support is sketchy at best.

I have tried most of the major PinePhone distros; Arch, PostmarketOS, Mobian, and finally Manjaro. I am still meaning to look at Ubuntu Touch, and I have messed with NixOS a little, but those are projects for another time. What I have found is that the software is just getting to a usable state if you are a seriously hard core nerd. It's incredibly slow and underpowered, but as far as phones go, I love it. What makes it special to me is the fact that it's open, extendable and hackable. I have a keyboard for it, but on the days I go snowboarding, I can opt to not bring it. It's an amazing little phone, and I don't know what else I would use for a phone.

The reason I first decided to try the PinePhone was because I got tired of Apple and Google's ecosystems. The "iPhone is the largest and most successful selling product in [the] entire Apple line up," with their panoply of iPhones to hit every price point, this is obvious (Reddy). Their idea is that you buy an iPhone, then to make it work better, you get AirPods, then you get iCloud, and so on and so forth. Google on the other hand has a business model based on ads and hardware sales. However, Apple's model of recurring revenue is far more profitable and Google has been scrambling to make their own version with features like "a system so that any messaging app on your phone can be mirrored on a Chromebook" (Bohn). This arms race to have the best ecosystem has gotten out of hand, and it's resulted in terrible user experience. Seeing as I was not tied to any one ecosystem, and was far more familiar with Linux than Android or iOS, I decided to give Linux a shot. Coincidently, Linux is working on  developing an ecosystem in and of its own. On SXMO "SSH [is] a first class citizen" and with Plasma Mobile, it integrates with Plasma Desktop via KDE Connect ("Simple X Mobile"). This allows quick file sharing, sms syncing, and even notifications sharing.

One of my favorite features of the phone is the keyboard, which I do not recommend getting at all. It has a built-in battery which broke on my model, the keys have a tendency to get stuck, it's super bulky, and you can't use your phone like a phone with it installed. However, the simple act of being able to use your phone with a physical keyboard is wonderful, and I will be looking into other options in the near future, the only limit being the fact that very few people compile firmware for I2C.

One other thing to look out for with this device is the fact that the PinePhone Pro is on the horizon. I plan on getting the Pro in a few months when most of the bugs are ironed out, but for most people, $400 for an experimental device is a lot to ask, and it's a somewhat irresponsible idea to get it even by my standards. However, the PinePhone is a platform I would love to see grow, and the ecosystem of Linux is something we must grow if we want to compete with big tech going into the future. The phone is the center of Apple and Google's ecosystems so I will continue using the PinePhone in an attempt to further the Linux ecosystem.

# Sources:
Bohn, Dieter. “Google Will Spend 2022 Trying to Match Apple's Ecosystem Integrations.” The Verge, The Verge, 5 Jan. 2022, https://www.theverge.com/2022/1/5/22864892/google-2022-ecosystem-android-chromebook-ces-features.
Reddy, Rajshekhar. “The Apple Ecosystem Explained.” Medium, Mac O'Clock, 23 Sept. 2021, https://medium.com/macoclock/the-apple-ecosystem-explained-c9dd6d00ec95.
“Simple X Mobile.” Sxmo, https://sxmo.org/.

